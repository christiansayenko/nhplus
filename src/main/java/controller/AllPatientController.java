package controller;

import datastorage.PatientDAO;
import datastorage.TreatmentDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Patient;
import service.PatientDataService;
import utils.DateConverter;
import datastorage.DAOFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

/**
 * The <code>AllPatientController</code> contains the entire logic of the <code>AllPatientView</code>.
 * It determines which data is displayed and how to react to events.
 */
public class AllPatientController {
    @FXML
    private TableView<Patient> tableView;
    @FXML
    private TableColumn<Patient, Integer> colID;
    @FXML
    private TableColumn<Patient, String> colFirstName;
    @FXML
    private TableColumn<Patient, String> colSurname;
    @FXML
    private TextField txtSurname;
    @FXML
    private TextField txtFirstname;
    @FXML
    private TableColumn<Patient, String> colDateOfBirth;
    @FXML
    private TableColumn<Patient, String> colCareLevel;
    @FXML
    private TableColumn<Patient, String> colRoom;

    @FXML
    private Button btnDelete;
    @FXML
    private Button btnAdd;
    @FXML
    private TextField txtBirthday;
    @FXML
    private TextField txtCarelevel;
    @FXML
    private TextField txtRoom;
    @FXML

    private PatientDAO dao;
    private PatientDataService patientDataService;
    private final ObservableList<Patient> tableviewContent = FXCollections.observableArrayList();
    public static final String PATIENT_TABLE_NAME = "patient";

    /**
     * Initializes the corresponding fields. Is called as soon as the corresponding FXML file is to be displayed.
     */
    public void initialize() {
        this.dao = DAOFactory.getDAOFactory().createPatientDAO();
        this.patientDataService = new PatientDataService();
        readAllAndShowInTableView();

        this.colID.setCellValueFactory(new PropertyValueFactory<>("pid"));
        //CellValueFactory zum Anzeigen der Daten in der TableView
        this.colFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        //CellFactory zum Schreiben innerhalb der Tabelle
        this.colFirstName.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colSurname.setCellValueFactory(new PropertyValueFactory<>("surname"));
        this.colSurname.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colDateOfBirth.setCellValueFactory(new PropertyValueFactory<>("dateOfBirth"));
        this.colDateOfBirth.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colCareLevel.setCellValueFactory(new PropertyValueFactory<>("careLevel"));
        this.colCareLevel.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colRoom.setCellValueFactory(new PropertyValueFactory<>("roomnumber"));
        this.colRoom.setCellFactory(TextFieldTableCell.forTableColumn());

        this.addContextMenu();
        //Anzeigen der Daten
        this.tableView.setItems(this.tableviewContent);
    }

    /**
     * Adds context menu on-click event to the patient table.
     */
    private void addContextMenu() {
        ContextMenu cm = new ContextMenu();
        MenuItem mi = new MenuItem("Behandlungsende");
        cm.getItems().add(mi);

        tableView.setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                Patient selectedPatient = tableView.getSelectionModel().getSelectedItem();

                if (selectedPatient == null) {
                    return;
                }

                cm.getItems().get(0).setOnAction(menuItemEvent -> handleSetLockedAt(selectedPatient));

                cm.show(tableView, event.getScreenX(), event.getScreenY());
            }
        });
    }

    /**
     * Handles new firstname value.
     * @param event event including the value that a user entered into the cell.
     */
    @FXML
    public void handleOnEditFirstname(TableColumn.CellEditEvent<Patient, String> event){
        event.getRowValue().setFirstName(event.getNewValue());
        doUpdate(event);
    }

    /**
     * Handles new surname value.
     * @param event event including the value that a user entered into the cell.
     */
    @FXML
    public void handleOnEditSurname(TableColumn.CellEditEvent<Patient, String> event){
        event.getRowValue().setSurname(event.getNewValue());
        doUpdate(event);
    }

    /**
     * Handles new birthdate value
     * @param event event including the value that a user entered into the cell.
     */
    @FXML
    public void handleOnEditDateOfBirth(TableColumn.CellEditEvent<Patient, String> event){
        event.getRowValue().setDateOfBirth(event.getNewValue());
        doUpdate(event);
    }

    /**
     * Handles new careLevel value.
     * @param event event including the value that a user entered into the cell.
     */
    @FXML
    public void handleOnEditCareLevel(TableColumn.CellEditEvent<Patient, String> event){
        event.getRowValue().setCareLevel(event.getNewValue());
        doUpdate(event);
    }

    /**
     * Handles new roomnumber value.
     * @param event event including the value that a user entered into the cell.
     */
    @FXML
    public void handleOnEditRoomnumber(TableColumn.CellEditEvent<Patient, String> event){
        event.getRowValue().setRoomnumber(event.getNewValue());
        doUpdate(event);
    }

    /**
     * Updates a patient by calling the update-Method in the {@link PatientDAO}.
     * @param t row to be updated by the user (includes the patient).
     */
    private void doUpdate(TableColumn.CellEditEvent<Patient, String> t) {
        try {
            dao.update(t.getRowValue());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calls readAll in {@link PatientDAO} and shows updated patients in the table.
     */
    private void readAllAndShowInTableView() {
        this.tableviewContent.clear();
        patientDataService.handlePatientData();
        List<Patient> allPatients;
        try {
            allPatients = dao.readAll(PATIENT_TABLE_NAME);
            this.tableviewContent.addAll(allPatients);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles a delete-click-event. Calls the delete methods in the {@link PatientDAO} and {@link TreatmentDAO}.
     */
    @FXML
    public void handleDeleteRow() {
        TreatmentDAO tDao = DAOFactory.getDAOFactory().createTreatmentDAO();
        Patient selectedItem = this.tableView.getSelectionModel().getSelectedItem();
        try {
            tDao.deleteByPid(selectedItem.getPid());
            dao.deleteById(selectedItem.getPid());
            this.tableView.getItems().remove(selectedItem);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles an add-click-event. Creates a patient and calls the create method in the {@link PatientDAO}.
     */
    @FXML
    public void handleAdd() {
        String surname = this.txtSurname.getText();
        String firstname = this.txtFirstname.getText();
        String birthday = this.txtBirthday.getText();
        LocalDate date = DateConverter.convertStringToLocalDate(birthday);
        String careLevel = this.txtCarelevel.getText();
        String room = this.txtRoom.getText();
        try {
            Patient p = new Patient(firstname, surname, date, careLevel, room, null);
            dao.create(p);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        readAllAndShowInTableView();
        clearTextFields();
    }

    /**
     * Handles a set-treatment-end-click-event.
     */
    @FXML
    public void handleSetLockedAt(Patient patient) {
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/LockPatientView.fxml"));
            AnchorPane pane = loader.load();
            Scene scene = new Scene(pane);
            //da die primaryStage noch im Hintergrund bleiben soll
            Stage stage = new Stage();
            LockPatientController controller = loader.getController();

            controller.initialize(this, stage, patient);

            stage.setScene(scene);
            stage.setResizable(false);
            stage.showAndWait();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes content from all text fields.
     */
    private void clearTextFields() {
        this.txtFirstname.clear();
        this.txtSurname.clear();
        this.txtBirthday.clear();
        this.txtCarelevel.clear();
        this.txtRoom.clear();
    }
}