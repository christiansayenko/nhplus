package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import service.AuthenticationService;

import java.io.IOException;

public class MainWindowController {

    @FXML
    private BorderPane mainBorderPane;

    @FXML
    private Button buttonCaregiver;

    @FXML
    private Button buttonPatient;

    @FXML
    private Button buttonTreatment;

    @FXML
    private TextField inputEmail;

    @FXML
    private PasswordField inputPassword;

    @FXML
    private Label labelLogin;

    @FXML
    private Button buttonLogin;

    private AuthenticationService authService;

    @FXML
    public void initialize(){
        this.authService = new AuthenticationService();
        this.buttonPatient.setDisable(true);
        this.buttonTreatment.setDisable(true);
        this.buttonCaregiver.setDisable(true);
    }

    /**
     * Makes sure user credentials are valid using the {@link AuthenticationService} and displays appropriate messages in the GUI.
     * If user successfully authenticates, makes rest of the application accessible.
     */
    @FXML
    private void login(){
        String email = this.inputEmail.getText();
        String password = this.inputPassword.getText();

        if (!authService.validCredentials(email, password)) {
            if (email.isEmpty() && password.isEmpty()) {
                this.labelLogin.setText("Bitte Login-Daten eingeben!");
            } else if (email.isEmpty()) {
                this.labelLogin.setText("Bitte geben Sie ihre E-Mail ein!");
            } else if (password.isEmpty()) {
                this.labelLogin.setText("Bitte geben Sie ihr Passwort ein!");
            } else {
                this.labelLogin.setText("Login fehlgeschlagen!");
            }

            return;
        }

        String message = String.format("Willkommen %s!", this.inputEmail.getText());
        this.labelLogin.setText(message);
        this.buttonPatient.setDisable(false);
        this.buttonTreatment.setDisable(false);
        this.buttonCaregiver.setDisable(false);
        this.inputEmail.setVisible(false);
        this.inputPassword.setVisible(false);
        this.buttonLogin.setVisible(false);
    }

    /**
     * Handles showing the <code>AllCaregiverView</code>. Called when selecting the corresponding sidebar item.
     */
    @FXML
    private void handleShowAllCaregiver() {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/AllCaregiverView.fxml"));
        try {
            mainBorderPane.setCenter(loader.load());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleShowAllPatient() {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/AllPatientView.fxml"));
        try {
            mainBorderPane.setCenter(loader.load());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleShowAllTreatments() {
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("/AllTreatmentView.fxml"));
        try {
            mainBorderPane.setCenter(loader.load());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
