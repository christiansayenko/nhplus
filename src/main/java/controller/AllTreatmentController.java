package controller;

import datastorage.CaregiverDAO;
import datastorage.PatientDAO;
import datastorage.TreatmentDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Caregiver;
import model.Patient;
import model.Treatment;
import datastorage.DAOFactory;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static controller.AllCaregiverController.CAREGIVER_TABLE_NAME;
import static controller.AllPatientController.PATIENT_TABLE_NAME;

/**
 * The <code>AllTreatmentController</code> contains the entire logic of the <code>AllTreatmentView</code>.
 * It determines which data is displayed and how to react to events.
 */
public class AllTreatmentController {
    @FXML
    private TableView<Treatment> tableView;
    @FXML
    private TableColumn<Treatment, Integer> colID;
    @FXML
    public TableColumn<Treatment, Integer> colCid;
    @FXML
    private TableColumn<Treatment, Integer> colPid;
    @FXML
    private TableColumn<Treatment, String> colDate;
    @FXML
    private TableColumn<Treatment, String> colBegin;
    @FXML
    private TableColumn<Treatment, String> colEnd;
    @FXML
    private TableColumn<Treatment, String> colDescription;
    @FXML
    private ComboBox<String> caregiverComboBox;
    @FXML
    private ComboBox<String> patientComboBox;
    @FXML
    private Button btnNewTreatment;
    @FXML
    private Button btnDelete;

    private final ObservableList<Treatment> tableviewContent =
            FXCollections.observableArrayList();
    private TreatmentDAO dao;
    private final ObservableList<String> caregiverComboBoxData =
            FXCollections.observableArrayList();
    private final ObservableList<String> patientComboBoxData =
            FXCollections.observableArrayList();
    private ArrayList<Caregiver> caregiverList;
    private ArrayList<Patient> patientList;
    private Main main;

    public static final String TREATMENT_TABLE_NAME = "treatment";

    /**
     * Initializes the corresponding fields. Is called as soon as the corresponding FXML file is to be displayed.
     */
    public void initialize() {
        readAllAndShowInTableView();
        caregiverComboBox.setItems(caregiverComboBoxData);
        caregiverComboBox.getSelectionModel().select(0);
        patientComboBox.setItems(patientComboBoxData);
        patientComboBox.getSelectionModel().select(0);

        this.colID.setCellValueFactory(new PropertyValueFactory<>("tid"));
        this.colCid.setCellValueFactory(new PropertyValueFactory<>("cid"));
        this.colPid.setCellValueFactory(new PropertyValueFactory<>("pid"));
        this.colDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        this.colBegin.setCellValueFactory(new PropertyValueFactory<>("begin"));
        this.colEnd.setCellValueFactory(new PropertyValueFactory<>("end"));
        this.colDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        this.tableView.setItems(this.tableviewContent);
        createPatientComboBoxData();
        createCaregiverComboBoxData();
    }

    /**
     * Calls readAll in {@link TreatmentDAO} and shows treatments in the table.
     */
    public void readAllAndShowInTableView() {
        this.tableviewContent.clear();
        caregiverComboBox.getSelectionModel().select(0);
        patientComboBox.getSelectionModel().select(0);
        this.dao = DAOFactory.getDAOFactory().createTreatmentDAO();
        List<Treatment> allTreatments;
        try {
            allTreatments = dao.readAll(TREATMENT_TABLE_NAME);
            this.tableviewContent.addAll(allTreatments);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Supplies data to the patient combo box using the {@link PatientDAO}.
     */
    private void createPatientComboBoxData(){
        PatientDAO dao = DAOFactory.getDAOFactory().createPatientDAO();
        try {
            patientList = (ArrayList<Patient>) dao.readAll(PATIENT_TABLE_NAME);
            this.patientComboBoxData.add("Alle");
            for (Patient patient: patientList) {
                this.patientComboBoxData.add(patient.getSurname());
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Supplies data to the caregiver combo box using the {@link CaregiverDAO}.
     */
    private void createCaregiverComboBoxData(){
        CaregiverDAO dao = DAOFactory.getDAOFactory().createCaregiverDAO();
        try {
            caregiverList = (ArrayList<Caregiver>) dao.readAll(CAREGIVER_TABLE_NAME);
            this.caregiverComboBoxData.add("Alle");
            for (Caregiver caregiver: caregiverList) {
                this.caregiverComboBoxData.add(caregiver.getSurname());
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
    }


    /**
     * Handles interactions with the patient combo box.
     */
    @FXML
    public void handlePatientComboBox(){
        String p = this.patientComboBox.getSelectionModel().getSelectedItem();
        this.tableviewContent.clear();
        this.dao = DAOFactory.getDAOFactory().createTreatmentDAO();
        List<Treatment> allTreatments;
        if(p.equals("Alle")){
            try {
                allTreatments= this.dao.readAll(TREATMENT_TABLE_NAME);
                this.tableviewContent.addAll(allTreatments);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        Patient patient = searchPatientInList(p);
        if (patient != null) {
            try {
                allTreatments = dao.readTreatmentsByPid(patient.getPid());
                this.tableviewContent.addAll(allTreatments);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Handles interactions with the caregiver combo box.
     */
    @FXML
    public void handleCaregiverComboBox(){
        String c = this.caregiverComboBox.getSelectionModel().getSelectedItem();
        this.tableviewContent.clear();
        this.dao = DAOFactory.getDAOFactory().createTreatmentDAO();
        List<Treatment> allTreatments;
        if(c.equals("Alle")){
            try {
                allTreatments= this.dao.readAll(TREATMENT_TABLE_NAME);
                this.tableviewContent.addAll(allTreatments);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        Caregiver caregiver = searchCaregiverInList(c);
        if (caregiver != null) {
            try {
                allTreatments = dao.readTreatmentsByCid(caregiver.getCid());
                this.tableviewContent.addAll(allTreatments);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Search for a caregiver.
     * @param surname of the caregiver to search.
     * @return caregiver if one is found. Otherwise, return null.
     */
    private Caregiver searchCaregiverInList(String surname){
        for (Caregiver caregiver : this.caregiverList) {
            if (caregiver.getSurname().equals(surname)) {
                return caregiver;
            }
        }
        return null;
    }

    /**
     * Search for a patient.
     * @param surname of the patient to search.
     * @return patient if one is found. Otherwise, return null.
     */
    private Patient searchPatientInList(String surname){
        for (Patient patient : this.patientList) {
            if (patient.getSurname().equals(surname)) {
                return patient;
            }
        }
        return null;
    }

    /**
     * Handles a delete-click-event. Calls the delete methods in the {@link TreatmentDAO}.
     */
    @FXML
    public void handleDelete(){
        int index = this.tableView.getSelectionModel().getSelectedIndex();
        Treatment t = this.tableviewContent.remove(index);
        TreatmentDAO dao = DAOFactory.getDAOFactory().createTreatmentDAO();
        try {
            dao.deleteById(t.getTid());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles an add-click-event. Creates a treatment and calls the create method in the {@link TreatmentDAO}.
     */
    @FXML
    public void handleNewTreatment() {
        try {
            String c = this.caregiverComboBox.getSelectionModel().getSelectedItem();
            String p = this.patientComboBox.getSelectionModel().getSelectedItem();
            Caregiver caregiver = searchCaregiverInList(c);
            Patient patient = searchPatientInList(p);
            newTreatmentWindow(caregiver, patient);
        }
        catch(NullPointerException e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information");
            alert.setHeaderText("Daten für neue Behandlung fehlen!");
            alert.setContentText("Wählen Sie über die Combobox einen Pfleger und Patienten aus!");
            alert.showAndWait();
        }
    }

    /**
     * Handles a double-click-event.
     */
    @FXML
    public void handleMouseClick(){
        tableView.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2 && (tableView.getSelectionModel().getSelectedItem() != null)) {
                int index = this.tableView.getSelectionModel().getSelectedIndex();
                Treatment treatment = this.tableviewContent.get(index);

                treatmentWindow(treatment);
            }
        });
    }

    /**
     * Displays the window used to create a new treatment.
     */
    public void newTreatmentWindow(Caregiver caregiver, Patient patient){
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/NewTreatmentView.fxml"));
            AnchorPane pane = loader.load();
            Scene scene = new Scene(pane);
            //da die primaryStage noch im Hintergrund bleiben soll
            Stage stage = new Stage();

            NewTreatmentController controller = loader.getController();
            controller.initialize(this, stage, caregiver, patient);

            stage.setScene(scene);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Displays the window used to view a specific treatment.
     */
    public void treatmentWindow(Treatment treatment){
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/TreatmentView.fxml"));
            AnchorPane pane = loader.load();
            Scene scene = new Scene(pane);
            //da die primaryStage noch im Hintergrund bleiben soll
            Stage stage = new Stage();
            TreatmentController controller = loader.getController();

            controller.initializeController(this, stage, treatment);

            stage.setScene(scene);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}