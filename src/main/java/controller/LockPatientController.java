package controller;

import datastorage.DAOFactory;
import datastorage.PatientDAO;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Patient;
import service.PatientDataService;

import java.sql.SQLException;
import java.time.LocalDate;

/**
 * The <code>LockPatientController</code> contains the entire logic of the <code>LockPatientView</code>.
 * It determines which data is displayed and how to react to events.
 */
public class LockPatientController {
    @FXML
    private Label lblLockTreatment;
    @FXML
    private DatePicker lockDatePicker;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnCancel;

    private PatientDataService patientDataService;
    private AllPatientController controller;
    private Patient patient;
    private Stage stage;

    /**
     * Initializes the corresponding fields. Is called as soon as the corresponding FXML file is to be displayed.
     */
    public void initialize(AllPatientController controller, Stage stage, Patient patient) {
        this.patientDataService = new PatientDataService();
        this.controller = controller;
        this.patient = patient;
        this.stage = stage;
        setInitialLockDate(patient);
    }

    /**
     * Sets initial lock-date used in the date picker.
     * This is either the patients lock-date if one is set or the current date.
     */
    private void setInitialLockDate(Patient patient) {
        LocalDate lockDate = patient.getLockedAt() != null ? patient.getLockedAt() : LocalDate.now();

        this.lockDatePicker.setValue(lockDate);
    }

    /**
     * Updates the patient using update in {@link PatientDAO}.
     */
    private void doUpdate(){
        PatientDAO dao = DAOFactory.getDAOFactory().createPatientDAO();

        try {
            dao.update(patient);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles saving a new lock date to the patient.
     */
    @FXML
    public void handleSave(){
        this.patient.setLockedAt(lockDatePicker.getValue());
        doUpdate();
        stage.close();

        if (lockDatePicker.getValue().isEqual(LocalDate.now())) {
            patientDataService.lockPatientData(patient);
            controller.initialize();
        }
    }

    /**
     * Handles exiting the lock date window.
     */
    @FXML
    public void handleCancel(){
        stage.close();
    }
}
