package service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import datastorage.ConnectionBuilder;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Service used for authenticating users.
 */
public class AuthenticationService {
    /**
     * Handles validating given credentials.
     * @param email
     * @param password
     * @return true if credentials match an existing user.
     */
    public boolean validCredentials(String email, String password) {
        Connection con = ConnectionBuilder.getConnection();
        String sqlString = String.format("SELECT * FROM User WHERE email = '%s'", email);
        boolean valid = false;

        try {
            Statement statement = con.createStatement();
            ResultSet result = statement.executeQuery(sqlString);

            if (result.next()) {
                String userPassword = result.getString(3);
                valid = BCrypt.verifyer().verify(password.toCharArray(), userPassword).verified;
            }
        }
        catch (SQLException a) {
            a.printStackTrace();
        }

        return valid;
    }
}
