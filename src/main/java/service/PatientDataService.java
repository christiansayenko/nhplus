package service;

import datastorage.*;
import model.*;

import java.time.LocalDate;
import java.util.List;

import static controller.AllPatientController.PATIENT_TABLE_NAME;

/**
 * Service to handle locking and deleting patient data.
 */
public class PatientDataService {
    PatientDAO patientDao = DAOFactory.getDAOFactory().createPatientDAO();
    TreatmentDAO treatmentDao = DAOFactory.getDAOFactory().createTreatmentDAO();
    LockedPatientDAO lockedPatientDao = DAOFactory.getDAOFactory().createLockedPatientDAO();
    LockedTreatmentDAO lockedTreatmentDao = DAOFactory.getDAOFactory().createLockedTreatmentDAO();

    /**
     * Iterates through all patient data and chooses appropriate action of necessary.
     */
    public void handlePatientData() {
        try {
            List<Patient> patients = patientDao.readAll(PATIENT_TABLE_NAME);
            List<LockedPatient> lockedPatients = lockedPatientDao.readAll("locked_patient");

            for (Patient patient : patients) {
                if (patient.getLockedAt() != null && dueForLock(patient)) {
                    lockPatientData(patient);
                }
            }

            for (LockedPatient lockedPatient : lockedPatients) {
                if (dueForDeletion(lockedPatient)) {
                    deletePatientData(lockedPatient);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Locks patient data of given patient.
     * @param patient to be locked.
     */
    public void lockPatientData(Patient patient) {
        try {
            // create new tables with locked data, remove data from accessible tables
            lockedPatientDao.create(new LockedPatient(patient));

            List<Treatment> treatments = treatmentDao.readTreatmentsByPid(patient.getPid());

            for (Treatment treatment : treatments) {
                lockedTreatmentDao.create(new LockedTreatment(treatment));
            }

            treatmentDao.deleteByPid(patient.getPid());
            patientDao.deleteById(patient.getPid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes patient data of given locked patient.
     * @param lockedPatient to be deleted.
     */
    public void deletePatientData(LockedPatient lockedPatient) {
        try {
            lockedTreatmentDao.deleteByLpid(lockedPatient.getLpid());
            lockedPatientDao.deleteById(lockedPatient.getLpid());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Determines if given patient is due for being locked.
     * @param patient to be checked.
     * @return true if due for lock.
     */
    private boolean dueForLock(Patient patient) {
        return patient.getLockedAt().isEqual(LocalDate.now()) ||
                patient.getLockedAt().isAfter(LocalDate.now());
    }

    /**
     * Determines if given locked patient is due for being deleted.
     * @param lockedPatient to be checked.
     * @return true if due for deletion.
     */
    private boolean dueForDeletion(LockedPatient lockedPatient) {
        return lockedPatient.getLockedAt().isEqual(LocalDate.now().plusYears(30)) ||
                lockedPatient.getLockedAt().isAfter(LocalDate.now().plusYears(30));
    }
}
