package datastorage;

import model.LockedTreatment;
import utils.DateConverter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 * Implements the Interface <code>DAOImp</code>. Overrides methods to generate specific locked-treatment-SQL-queries.
 */
public class LockedTreatmentDAO extends DAOimp<LockedTreatment> {

    /**
     * Constructs Object. Calls the Constructor from <code>DAOImp</code> to store the connection.
     * @param conn the connection.
     */
    public LockedTreatmentDAO(Connection conn) {
        super(conn);
    }

    /**
     * Generates a <code>INSERT INTO</code>-Statement for a given treatment that is being locked.
     * @param treatment for which a specific INSERT INTO is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getCreateStatementString(LockedTreatment treatment) {
        return String.format("INSERT INTO locked_treatment (ltid, lpid, treatment_date, begin, end, description, remarks, caregiver_name) VALUES " +
                        "(%d, %d, '%s', '%s', '%s', '%s', '%s', '%s')", treatment.getLtid(), treatment.getLpid(), treatment.getDate(),
                treatment.getBegin(), treatment.getEnd(), treatment.getDescription(), treatment.getRemarks(), treatment.getCaregiverName());
    }

    /**
     * Generates a <code>SELECT</code>-Statement for a given key.
     * @param key for which a specific SELECT is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM locked_treatment WHERE ltid = %d", key);
    }

    /**
     * Deletes all locked treatments by given locked patient id.
     * @param lpid of the locked patient.
     */
    public void deleteByLpid(long lpid) throws SQLException {
        Statement st = conn.createStatement();
        st.executeUpdate(String.format("Delete FROM locked_treatment WHERE lpid = %d", lpid));
    }

    /**
     * Generates a <code>delete</code>-Statement for a given key.
     * @param key for which a specific DELETE is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM locked_treatment WHERE ltid = %d", key);
    }

    /**
     * Required implementation of DAOimp method.
     */
    @Override
    protected LockedTreatment getInstanceFromResultSet(ResultSet result) {
       return null;
    }

    /**
     * Maps a <code>ResultSet</code> to a <code>LockedTreatment-List</code>
     * @param result ResultSet with a multiple rows. Data will be mapped to LockedTreatment-object.
     * @return ArrayList with locked treatments from the resultSet.
     */
    @Override
    protected ArrayList<LockedTreatment> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<LockedTreatment> list = new ArrayList<>();
        LockedTreatment t;
        while (result.next()) {
            LocalDate date = DateConverter.convertStringToLocalDate(result.getString(3));
            LocalTime begin = DateConverter.convertStringToLocalTime(result.getString(4));
            LocalTime end = DateConverter.convertStringToLocalTime(result.getString(5));
            t = new LockedTreatment(result.getLong(1), result.getLong(2), date, begin, end,
                    result.getString(6), result.getString(7), result.getString(8));
            list.add(t);
        }
        return list;
    }

    /**
     * Required implementation of DAOimp method.
     */
    @Override
    protected String getUpdateStatementString(LockedTreatment treatment) {
        return null;
    }
}
