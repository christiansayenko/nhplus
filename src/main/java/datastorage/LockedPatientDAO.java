package datastorage;

import model.LockedPatient;
import utils.DateConverter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Implements the Interface <code>DAOImp</code>. Overrides methods to generate specific locked-patient-SQL-queries.
 */
public class LockedPatientDAO extends DAOimp<LockedPatient> {
    
    /**
     * Constructs Object. Calls the Constructor from <code>DAOImp</code> to store the connection.
     * @param conn the connection.
     */
    public LockedPatientDAO(Connection conn) {
        super(conn);
    }

    /**
     * Generates a <code>INSERT INTO</code>-Statement for a given patient that is being locked.
     * @param patient for which a specific INSERT INTO is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getCreateStatementString(LockedPatient patient) {
        return String.format("INSERT INTO locked_patient (lpid, firstname, surname, dateOfBirth, locked_at) VALUES (%d, '%s', '%s', '%s', '%s')",
                patient.getLpid(), patient.getFirstName(), patient.getSurname(), patient.getDateOfBirth(), patient.getLockedAt());
    }

    /**
     * Generates a <code>SELECT</code>-Statement for a given key.
     * @param key for which a specific SELECT is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM locked_patient WHERE lpid = %d", key);
    }

    /**
     * Generates a <code>delete</code>-Statement for a given key.
     * @param key for which a specific DELETE is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM locked_patient WHERE lpid = %d", key);
    }

    /**
     * Required implementation of DAOimp method.
     */
    @Override
    protected LockedPatient getInstanceFromResultSet(ResultSet result) {
        return null;
    }

    /**
     * Maps a <code>ResultSet</code> to a <code>LockedPatient-List</code>
     * @param result ResultSet with a multiple rows. Data will be mapped to LockedPatient-object.
     * @return ArrayList with locked patients from the resultSet.
     */
    @Override
    protected ArrayList<LockedPatient> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<LockedPatient> list = new ArrayList<>();

        while (result.next()) {
            LocalDate birthDay = DateConverter.convertStringToLocalDate(result.getString(4));
            LocalDate lockedAt = DateConverter.convertStringToLocalDate(result.getString(5));

            list.add(new LockedPatient(result.getLong(1), result.getString(2),
                    result.getString(3), birthDay, lockedAt));
        }
        return list;
    }

    /**
     * Required implementation of DAOimp method.
     */
    @Override
    protected String getUpdateStatementString(LockedPatient patient) {
        return null;
    }
}
