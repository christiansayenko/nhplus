package datastorage;

import model.Caregiver;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Implements the Interface <code>DAOImp</code>. Overrides methods to generate specific caregiver-SQL-queries.
 */
public class CaregiverDAO extends DAOimp<Caregiver> {

    /**
     * Calls the Constructor from <code>DAOImp</code> to store the connection.
     * @param conn the connection.
     */
    public CaregiverDAO(Connection conn) {
        super(conn);
    }

    /**
     * Generates a <code>INSERT INTO</code>-Statement for a given Caregiver.
     * @param caregiver for which a specific INSERT INTO is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getCreateStatementString(Caregiver caregiver) {
        return String.format("INSERT INTO caregiver (firstname, surname, phonenumber) VALUES ('%s', '%s', '%s')",
                caregiver.getFirstName(), caregiver.getSurname(), caregiver.getPhoneNumber());
    }

    /**
     * Generates a <code>SELECT</code>-Statement for a given key.
     * @param key for which a specific SELECT is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM caregiver WHERE cid = %d LIMIT 1", key);
    }

    /**
     * Maps a <code>ResultSet</code> to a <code>Caregiver</code>.
     * @param result ResultSet with a single row. Columns will be mapped to a caregiver-object.
     * @return caregiver with the data from the resultSet.
     */
    @Override
    protected Caregiver getInstanceFromResultSet(ResultSet result) throws SQLException {
        return new Caregiver(result.getInt(1), result.getString(2),
                result.getString(3), result.getString(4));
    }

    /**
     * Maps a <code>ResultSet</code> to a <code>Caregiver-List</code>
     * @param result ResultSet with a multiple rows. Data will be mapped to Caregiver-object.
     * @return ArrayList with caregivers from the resultSet.
     */
    @Override
    protected ArrayList<Caregiver> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Caregiver> list = new ArrayList<>();
        while (result.next()) {
            list.add(
                new Caregiver(result.getInt(1), result.getString(2),
                    result.getString(3), result.getString(4))
            );
        }
        return list;
    }

    /**
     * Generates a <code>UPDATE</code>-Statement for a given caregiver.
     * @param caregiver for which a specific update is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getUpdateStatementString(Caregiver caregiver) {
        return String.format("UPDATE caregiver SET firstname = '%s', surname = '%s', phonenumber = '%s' WHERE cid = %d",
                caregiver.getFirstName(), caregiver.getSurname(), caregiver.getPhoneNumber(), caregiver.getCid());
    }

    /**
     * Generates a <code>delete</code>-Statement for a given key.
     * @param key for which a specific DELETE is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM caregiver WHERE cid = %d", key);
    }
}
