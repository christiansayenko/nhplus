package datastorage;

import model.Patient;
import utils.DateConverter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Implements the Interface <code>DAOImp</code>. Overrides methods to generate specific patient-SQL-queries.
 */
public class PatientDAO extends DAOimp<Patient> {

    /**
     * Constructs Object. Calls the Constructor from <code>DAOImp</code> to store the connection.
     * @param conn the connection.
     */
    public PatientDAO(Connection conn) {
        super(conn);
    }

    /**
     * Generates a <code>INSERT INTO</code>-Statement for a given patient.
     * @param patient for which a specific INSERT INTO is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getCreateStatementString(Patient patient) {
        return String.format("INSERT INTO patient (firstname, surname, dateOfBirth, carelevel, roomnumber) VALUES ('%s', '%s', '%s', '%s', '%s')",
                patient.getFirstName(), patient.getSurname(), patient.getDateOfBirth(), patient.getCareLevel(), patient.getRoomnumber());
    }

    /**
     * Generates a <code>SELECT</code>-Statement for a given key.
     * @param key for which a specific SELECT is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getReadByIDStatementString(long key) {
        return String.format("SELECT * FROM patient WHERE pid = %d", key);
    }

    /**
     * Maps a <code>ResultSet</code> to a <code>Patient</code>.
     * @param result ResultSet with a single row. Columns will be mapped to a patient-object.
     * @return patient with the data from the resultSet.
     */
    @Override
    protected Patient getInstanceFromResultSet(ResultSet result) throws SQLException {
        LocalDate birthDay = DateConverter.convertStringToLocalDate(result.getString(4));
        LocalDate lockedAt = DateConverter.convertStringToLocalDate(result.getString(7));

       return new Patient(result.getInt(1), result.getString(2),
                result.getString(3), birthDay , result.getString(5),
                result.getString(6), lockedAt);
    }

    /**
     * Maps a <code>ResultSet</code> to a <code>Patient-List</code>
     * @param result ResultSet with a multiple rows. Data will be mapped to patient-object.
     * @return ArrayList with patients from the resultSet.
     */
    @Override
    protected ArrayList<Patient> getListFromResultSet(ResultSet result) throws SQLException {
        ArrayList<Patient> list = new ArrayList<>();

        while (result.next()) {
            LocalDate birthDay = DateConverter.convertStringToLocalDate(result.getString(4));
            LocalDate lockedAt = DateConverter.convertStringToLocalDate(result.getString(7));

            list.add(new Patient(result.getLong(1), result.getString(2),
                    result.getString(3), birthDay, result.getString(5),
                    result.getString(6), lockedAt));
        }
        return list;
    }

    /**
     * Generates a <code>UPDATE</code>-Statement for a given patient.
     * @param patient for which a specific update is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getUpdateStatementString(Patient patient) {
        return String.format("UPDATE patient SET firstname = '%s', surname = '%s', dateOfBirth = '%s', carelevel = '%s', " +
                        "roomnumber = '%s', locked_at = '%s' WHERE pid = %d", patient.getFirstName(), patient.getSurname(), patient.getDateOfBirth(),
                        patient.getCareLevel(), patient.getRoomnumber(), patient.getLockedAt(), patient.getPid());
    }

    /**
     * Generates a <code>delete</code>-Statement for a given key.
     * @param key for which a specific DELETE is to be created.
     * @return <code>String</code> with the generated SQL.
     */
    @Override
    protected String getDeleteStatementString(long key) {
        return String.format("Delete FROM patient WHERE pid = %d", key);
    }
}
