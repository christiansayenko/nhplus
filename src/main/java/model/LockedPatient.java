package model;

import java.time.LocalDate;

/**
 * Locked patients represent patients that are no longer at the nursing home.
 */
public class LockedPatient extends Person {
    private long lpid;
    private LocalDate dateOfBirth;
    private LocalDate lockedAt;

    /**
     * Constructs a locked patient from the given params.
     * @param firstName
     * @param surname
     * @param dateOfBirth
     * @param lockedAt
     */
    public LockedPatient(
            long lpid,
            String firstName,
            String surname,
            LocalDate dateOfBirth,
            LocalDate lockedAt
    ) {
        super(firstName, surname);
        this.lpid = lpid;
        this.dateOfBirth = dateOfBirth;
        this.lockedAt = lockedAt;
    }

    /**
     * Constructs a locked patient from an existing patient.
     * @param patient
     */
    public LockedPatient(Patient patient) {
        super(patient.getFirstName(), patient.getSurname());
        this.lpid = patient.getPid();
        this.dateOfBirth = LocalDate.parse(patient.getDateOfBirth());
        this.lockedAt = patient.getLockedAt();
    }

    /**
     * @return locked patient id.
     */
    public long getLpid() {
        return lpid;
    }

    /**
     * @return date of birth.
     */
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth new date of birth.
     */
    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return locked at date.
     */
    public LocalDate getLockedAt() {
        return lockedAt;
    }

    /**
     * @param lockedAt new locked at date.
     */
    public void setLockedAt(LocalDate lockedAt) {
        this.lockedAt = lockedAt;
    }
}
