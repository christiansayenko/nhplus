package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Caregivers work at the nursing home and treat the patients.
 */
public class Caregiver extends Person {
    private long cid;
    private String phoneNumber;
    private final List<Treatment> allTreatments = new ArrayList<>();

    /**
     * Constructs a caregiver from the given params.
     * @param firstName
     * @param surName
     * @param phoneNumber
     */
    public Caregiver(String firstName, String surName, String phoneNumber) {
        super(firstName, surName);
        this.phoneNumber = phoneNumber;
    }

    /**
     * Constructs a caregiver from the given params.
     * @param cid
     * @param firstName
     * @param surName
     * @param phoneNumber
     */
    public Caregiver(long cid, String firstName, String surName, String phoneNumber) {
        super(firstName, surName);
        this.cid = cid;
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return Caregiver-Id
     */
    public long getCid() {
        return cid;
    }

    /**
     * @return Phone number.
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber New phone number.
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Adds a treatment to the treatment-list, if it does not already contain it.
     * @param treatment
     * @return True if the treatment was not already part of the list. Otherwise, return false.
     */
    public boolean add(Treatment treatment) {
        if (!this.allTreatments.contains(treatment)) {
            this.allTreatments.add(treatment);
            return true;
        }
        return false;
    }
}
