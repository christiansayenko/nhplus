package model;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Locked treatments represent treatments of patients that are no longer at the nursing home.
 */
public class LockedTreatment {
    private long ltid;
    private long lpid;
    private LocalDate date;
    private LocalTime begin;
    private LocalTime end;
    private String description;
    private String remarks;
    private String caregiverName;

    /**
     * Constructs a locked treatment from the given params.
     * @param ltid
     * @param lpid
     * @param date
     * @param begin
     * @param end
     * @param description
     * @param remarks
     * @param caregiverName
     */
    public LockedTreatment(
        long ltid,
        long lpid,
        LocalDate date,
        LocalTime begin,
        LocalTime end,
        String description,
        String remarks,
        String caregiverName
    ) {
        this.ltid = ltid;
        this.lpid = lpid;
        this.date = date;
        this.begin = begin;
        this.end = end;
        this.description = description;
        this.remarks = remarks;
        this.caregiverName = caregiverName;
    }

    /**
     * Constructs a locked treatment from an existing treatment.
     * @param treatment
     */
    public LockedTreatment(Treatment treatment) {
        this.ltid = treatment.getTid();
        this.lpid = treatment.getPid();
        this.date = LocalDate.parse(treatment.getDate());
        this.begin = LocalTime.parse(treatment.getBegin());
        this.end = LocalTime.parse(treatment.getEnd());
        this.description = treatment.getDescription();
        this.remarks = treatment.getRemarks();
        this.caregiverName = treatment.getCaregiverName();
    }

    /**
     * @return locked treatment id.
     */
    public long getLtid() {
        return ltid;
    }

    /**
     * @return locked patient id.
     */
    public long getLpid() {
        return lpid;
    }

    /**
     * @return treatment date.
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @param date new treatment date.
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return beginning date.
     */
    public LocalTime getBegin() {
        return begin;
    }

    /**
     * @param begin new beginning date.
     */
    public void setBegin(LocalTime begin) {
        this.begin = begin;
    }

    /**
     * @return end date.
     */
    public LocalTime getEnd() {
        return end;
    }

    /**
     * @param end new end date.
     */
    public void setEnd(LocalTime end) {
        this.end = end;
    }

    /**
     * @return description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description new description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return remarks.
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks new remarks.
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return caregiver name.
     */
    public String getCaregiverName() {
        return caregiverName;
    }

    /**
     * @param caregiverName new caregiver name
     */
    public void setCaregiverName(String caregiverName) {
        this.caregiverName = caregiverName;
    }
}
